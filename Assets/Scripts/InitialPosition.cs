using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitialPosition : MonoBehaviour
{
    Vector3 initialPosition;
    [SerializeField] private float inilat;
    [SerializeField] private float inilong;
    [SerializeField] private float finallat;
    [SerializeField] private float finallong;
    private float anx;
    private float any;
    public float multy = 27500;
    public float multx = -7000;

    private void Start()
    {
        initialPosition = transform.position;
        if(inilong>finallong && inilat > finallat){
            multx = 7*multx;
        }
        else if(inilong<finallong && inilat<finallat){
            multx = -multx*7;
        }
        else if(inilong>finallong && inilat < finallat){
            multx = -multx*2;
        }
    }
    private void Update()
    {
        // finallat = (GPS.Instance.latitude);
        // finallong = (GPS.Instance.longitude);
        anx = -multx*(finallong-inilong);
        any = -multy*(finallat-inilat);
        // finallat = (GPS.Instance.latitude);
        // finallong = (GPS.Instance.longitude);
        // Calculate the new position of the game object based on GPS coordinates
        Vector3 newPosition = new Vector3(anx, initialPosition.y, any);
        transform.position = newPosition;
    }
}
